#include <iostream>
#include <glm/mat4x4.hpp>

#include <boost/program_options.hpp>

//Using SDL and standard IO
#include <SDL.h>
#include <SDL_main.h>
#include <functional>
#include <memory>
#include <glm/mat4x4.hpp>
#include <stdexcept>
#include <regex>

#include "Screen.h"
#include "event/EventSource.h"
#include "model/CameraModel.h"
#include "model/RotationModel.h"
#include "model/ButtonHandler.h"
#include "model/PointHolderThing.h"
#include "loading/FileLoader.h"

glm::vec3 rotate(glm::vec3 point, glm::mat4 rotationMat) {
    return glm::vec4(point, 1) * rotationMat;
}

boost::program_options::variables_map handleArgs(int argc, char * argv[]) {
    // Declare the supported options.
    boost::program_options::options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce help message")
            ("obj_points_file,f", boost::program_options::value<std::string>(), "point to an obj_points formatted file to load")
            ;

    boost::program_options::variables_map variablesMap;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), variablesMap);
    boost::program_options::notify(variablesMap);

    if (variablesMap.count("help")) {
        std::cout << desc << "\n";
        std::exit(EXIT_SUCCESS);
    }

    if (variablesMap.count("obj_points_file")) {
        // do whatever
    } else {
        std::cout << "Please specify a voxel data file to load (using the obj_points_file option).\n";
    }

    return variablesMap;
}

int main(int argc, char * argv[]) {
    auto variablesMap = handleArgs(argc, argv);

    FileLoader fileLoader = FileLoader();
    auto lines = *(fileLoader.loadFile(variablesMap["obj_points_file"].as<std::string>()));

    PointHolderThing pointHolderThing = PointHolderThing();

    pointHolderThing.parseLines(lines);

    Screen* screen = nullptr;
    try {
        screen = Screen::MakeScreen();
    }
    catch ( const std::runtime_error& e ) {
        SDL_Quit();
        std::cout << e.what();
        return EXIT_FAILURE;
    }

    screen->initial_render();

    int timer = 0;

    std::unique_ptr<CameraModel> cameraModel = std::make_unique<CameraModel>();
    std::unique_ptr<RotationModel> rotationModel = std::make_unique<RotationModel>();
    std::unique_ptr<ButtonHandler> buttonHandler = std::make_unique<ButtonHandler>();

    EventSource events = EventSource();
    events.addObserver(cameraModel.get());
    events.addObserver(rotationModel.get());
    events.addObserver(buttonHandler.get());

    std::vector<glm::vec3> points = pointHolderThing.getPoints();

    while(!buttonHandler->shouldQuit()) {
        timer++;

        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            events.notifyObservers(event);
        }

        std::vector<glm::vec3> rotatedPoints = std::vector<glm::vec3>();
        for (glm::vec3 point : points) {
            rotatedPoints.push_back(rotate(point, rotationModel->getRotMat()));
        }

        screen->render(cameraModel.get(), timer, rotatedPoints,
                rotationModel.get());
    }

    screen->destroy();
    return EXIT_SUCCESS;
}
