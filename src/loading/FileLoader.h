#ifndef WAHUM_OBJPOINTSPARSER_H
#define WAHUM_OBJPOINTSPARSER_H

#include <string>
#include <vector>

class FileLoader {
public:
    std::vector<std::string> * loadFile(std::string path);
};


#endif //WAHUM_OBJPOINTSPARSER_H