#include "FileLoader.h"

#include <string>
#include <iostream>
#include <fstream>
#include <vector>

std::vector<std::string> * FileLoader::loadFile(const std::string path) {
    std::string line;
    std::ifstream myfile (path);

    auto retVal = new std::vector<std::string>();

    if (myfile.is_open()) {
        while (getline(myfile,line)) {
            retVal->push_back(line);
        }
        myfile.close();
    } else {
        std::cout << "Unable to open file";
    }

    return retVal;
}