#ifndef WAHUM_SDLCONTEXT_H
#define WAHUM_SDLCONTEXT_H

#include <glm/mat4x4.hpp>
#include <vector>

#include "model/RotationModel.h"
#include "model/CameraModel.h"

class Screen {
public:
    static Screen* MakeScreen();
    void render(CameraModel *cameraPosition, int timer, std::vector<glm::vec3> points,
            RotationModel *rotationModel);
    void initial_render();
    void destroy();
private:
    Screen(SDL_Texture *, Uint32 *, SDL_Window *, SDL_Renderer *, SDL_Rect *in_src_rect);
    SDL_Texture* m_texture;
    Uint32* m_buffer;
    SDL_Window* m_window;
    SDL_Renderer* m_renderer;
    SDL_Rect* m_src_rect;

    struct ScreenCoords {
        ScreenCoords(int x, int y) {
            this->x = x;
            this->y = y;
        }
        int x,y;
    };
    static ScreenCoords * screenSpaceTransform(glm::vec3 *worldCameraTranslated);
    void draw(glm::mat4 cameraViewMatrix, glm::vec3 point);
    static glm::vec3 cameraWorldTransform(glm::vec3);

    static glm::mat4 makeCameraViewMatrix(glm::vec3 position);
    static glm::mat4 invert(glm::mat4 matrix);
    static glm::vec3* makeProjectionMatrix(glm::vec3 threeDCoords);
};

#endif //WAHUM_SDLCONTEXT_H
