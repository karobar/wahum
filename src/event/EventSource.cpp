#include "EventSource.h"

void EventSource::addObserver(Observer * observer) {
    observers.push_back(observer);
}

void EventSource::notifyObservers(SDL_Event SDL_Event) {
    for (Observer * observer : observers) {
        observer->update(SDL_Event);
    }
}
