#ifndef WAHUM_EVENTSOURCE_H
#define WAHUM_EVENTSOURCE_H

#include <vector>
#include <memory>
#include "Observer.h"

class EventSource {
    public:
        void addObserver(Observer * observer);
        void notifyObservers(SDL_Event SDL_Event);
    private:
        std::vector<Observer *> observers;
};

#endif //WAHUM_EVENTSOURCE_H
