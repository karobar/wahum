#ifndef WAHUM_OBSERVER_H
#define WAHUM_OBSERVER_H

#include <SDL.h>

class Observer {
public:
    virtual void update(SDL_Event) = 0;
};

#endif //WAHUM_OBSERVER_H
