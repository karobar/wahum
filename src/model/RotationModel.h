#ifndef WAHUM_MESHMODEL_H
#define WAHUM_MESHMODEL_H

#include <glm/mat4x4.hpp>

#include "../event/Observer.h"

class RotationModel : public Observer {
public:
    void update(SDL_Event);
    glm::mat4 getRotMat();
private:
    glm::mat4 getXRotMat();
    glm::mat4 getZRotMat();
    double xTheta;
    double zTheta;
    constexpr static const double INPUT_FACTOR = 0.1;
};

#endif //WAHUM_MESHMODEL_H
