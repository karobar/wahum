#include "ButtonHandler.h"

bool ButtonHandler::shouldQuit() {
    return this->shouldQuitInternal;
}

void ButtonHandler::update(SDL_Event event) {
    if(event.type == SDL_QUIT) {
        this->shouldQuitInternal = true;
    }
}