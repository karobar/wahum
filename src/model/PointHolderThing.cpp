#include <vector>
#include <string>
#include <glm/mat4x4.hpp>
#include <regex>
#include <iostream>
#include <glm/gtx/transform.hpp>

#include "PointHolderThing.h"

std::vector<glm::vec3> PointHolderThing::getPoints() {
    if (points.empty()) {
        return {
                glm::vec3(-0.5, -0.5, -0.5),
                glm::vec3(-0.5, -0.5, -0.5),
                glm::vec3(-0.5, -0.5, 0.5),
                glm::vec3(-0.5, 0.5, -0.5),
                glm::vec3(-0.5, 0.5, 0.5),
                glm::vec3(0.5, -0.5, -0.5),
                glm::vec3(0.5, -0.5, 0.5),
                glm::vec3(0.5, 0.5, -0.5),
                glm::vec3(0.5, 0.5, 0.5),
        };
    } else {
        return points;
    }
}

std::vector<glm::vec3> scaleDownPoints(std::vector<glm::vec3> points,
        glm::vec3 scalingFactor, glm::vec3 centroid) {
    std::vector<glm::vec3> scaledDownPoints;

    for (glm::vec3 point : points) {
        scaledDownPoints.emplace_back(glm::vec3(
                point.x * scalingFactor.x,
                point.y * scalingFactor.y,
                point.z * scalingFactor.z) - centroid);
    }

    return scaledDownPoints;
}

void PointHolderThing::parseLines(const std::vector<std::string> & lines) {
    std::vector<glm::vec3> rawPoints;
    double maxX = 0;
    double maxY = 0;
    double maxZ = 0;

    double totalX = 0;
    double totalY = 0;
    double totalZ = 0;

    unsigned int count = 0;

    for (const std::string & line : lines) {
        std::smatch stringMatch;
        regex_search(line, stringMatch, std::regex("v (.*) (.*) (.*)"));

        std::string x_str = stringMatch[1];
        std::string y_str = stringMatch[2];
        std::string z_str = stringMatch[3];

        double x = std::strtod(x_str.c_str(), nullptr);
        if (std::abs(x) > maxX) {
            maxX = x;
        }
        totalX += x;

        double y = std::strtod(y_str.c_str(), nullptr);
        if (std::abs(y) > maxY) {
            maxY = y;
        }
        totalY += y;

        double z = std::strtod(z_str.c_str(), nullptr);
        if (std::abs(z) > maxZ) {
            maxZ = z;
        }
        totalZ += z;

        count++;

        rawPoints.emplace_back(glm::vec3(x,y,z));
    }

    glm::vec3 scalingVector = glm::vec3(1.0 / maxX, 1.0 / maxY, 1.0 / maxZ);

    glm::vec3 centroid = glm::vec3(
            totalX / (count * maxX),
            totalY / (count * maxY),
            totalZ / (count * maxZ));

    points = scaleDownPoints(rawPoints, scalingVector, centroid);
}
