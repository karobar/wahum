#ifndef WAHUM_POINTHOLDERTHING_H
#define WAHUM_POINTHOLDERTHING_H


class PointHolderThing {
public:
    std::vector<glm::vec3> getPoints();
    void parseLines(const std::vector<std::string> & lines);
private:
    std::vector<glm::vec3> getDefaultCubePoints();
    std::vector<glm::vec3> points;
};


#endif //WAHUM_POINTHOLDERTHING_H
