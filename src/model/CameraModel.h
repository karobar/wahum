#ifndef WAHUM_CAMERAMODEL_H
#define WAHUM_CAMERAMODEL_H

#include <glm/mat4x4.hpp>
#include "../event/Observer.h"

class CameraModel : public Observer {
public:
    void update(SDL_Event);
public:
    glm::vec3 translation = glm::vec3(0, 0, 0);
private:
    constexpr static const double TRANSLATION_FACTOR = 0.1;
};


#endif //WAHUM_CAMERAMODEL_H
