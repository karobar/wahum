#include "RotationModel.h"

void RotationModel::update(SDL_Event event) {
    if (event.type == SDL_KEYDOWN) {
        switch( event.key.keysym.sym ) {
            case SDLK_q:
                this->xTheta -= INPUT_FACTOR;
                break;
            case SDLK_e:
                this->xTheta += INPUT_FACTOR;
                break;
            case SDLK_z:
                this->zTheta -= INPUT_FACTOR;
                break;
            case SDLK_c:
                this->zTheta += INPUT_FACTOR;
                break;
            default:
                break;
        }
    }
}

glm::mat4 RotationModel::getRotMat() {
    return getXRotMat() * getZRotMat();
}

glm::mat4 RotationModel::getXRotMat() {
    return glm::mat4(
     1, 0, 0, 0,
     0, cosf(xTheta), -sinf(xTheta), 0,
     0, sin(xTheta), cos(xTheta), 0,
     0, 0, 0, 1);
}

glm::mat4 RotationModel::getZRotMat() {
    return glm::mat4(
            cosf(zTheta), -sin(zTheta), 0, 0,
            sin(zTheta), cosf(zTheta), 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);
}