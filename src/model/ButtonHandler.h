#ifndef WAHUM_BUTTONHANDLER_H
#define WAHUM_BUTTONHANDLER_H

#include "../event/Observer.h"

class ButtonHandler : public Observer {
public:
    void update(SDL_Event event);
    bool shouldQuit();
private:
    bool shouldQuitInternal = false;
};

#endif //WAHUM_BUTTONHANDLER_H
