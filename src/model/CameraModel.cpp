
#include "CameraModel.h"



void CameraModel::update(SDL_Event event) {
    switch (event.type) {
        /* Look for a keypress */
        case SDL_KEYDOWN:
            switch( event.key.keysym.sym ) {
                case SDLK_LEFT:
                    this->translation += glm::vec3(-CameraModel::TRANSLATION_FACTOR, 0, 0);
                    break;
                case SDLK_RIGHT:
                    this->translation += glm::vec3(CameraModel::TRANSLATION_FACTOR, 0, 0);
                    break;
                case SDLK_SPACE:
                    this->translation += glm::vec3(0, CameraModel::TRANSLATION_FACTOR, 0);
                    break;
                case SDLK_LCTRL:
                    this->translation += glm::vec3(0, -CameraModel::TRANSLATION_FACTOR, 0);
                    break;
                case SDLK_UP:
                    this->translation += glm::vec3(0, 0, -CameraModel::TRANSLATION_FACTOR);
                    break;
                case SDLK_DOWN:
                    this->translation += glm::vec3(0, 0, CameraModel::TRANSLATION_FACTOR);
                    break;
                default:
                    break;
            }
            break;
    }
}