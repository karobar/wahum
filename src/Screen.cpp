#include <SDL2/SDL.h>
#include <cmath>
#include <iostream>
#include <string.h>
#include <glm/mat4x4.hpp>

#include <stdio.h>
#include <cmath>
#include <memory>
#include "Screen.h"
#include "model/RotationModel.h"
#include "model/CameraModel.h"
#include <glm/gtc/matrix_transform.hpp>
#include <vector>


const int CHARSHEET_WIDTH = 16;
const int CELL_PIXEL_WIDTH = 8;
const int CELL_PIXEL_HEIGHT = 12;

//Screen dimension constants
const static int SCREEN_CELL_WIDTH = 80;
const static int SCREEN_CELL_HEIGHT = 40;

const int SCREEN_WIDTH = CELL_PIXEL_WIDTH * SCREEN_CELL_WIDTH;
const int SCREEN_HEIGHT = CELL_PIXEL_HEIGHT * SCREEN_CELL_HEIGHT;

const static float DEFAULT_FOV_RADIANS = 5 / (float) 2;

Screen::Screen(SDL_Texture *in_texture, Uint32 *in_buffer, SDL_Window *in_window, SDL_Renderer *in_renderer,
               SDL_Rect *in_src_rect) {
    m_texture = in_texture;
    m_buffer = in_buffer;
    m_window = in_window;
    m_renderer = in_renderer;
    m_src_rect = in_src_rect;
}

void Screen::destroy() {
    SDL_DestroyTexture(m_texture);
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyWindow(m_window);
    SDL_Quit();
}

SDL_Window* MakeWindow() {
    SDL_Window* window = SDL_CreateWindow("Wahum", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
                                          SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if(window == nullptr) {
        throw std::runtime_error("Failed to init m_window!");
    }
    return window;
}

SDL_Renderer* MakeRenderer(SDL_Window* window) {
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);
    if(renderer == nullptr) {
        throw std::runtime_error("Failed to init m_renderer!");
    }
    return renderer;
}

Uint32* MakeBuffer() {
    auto buffer = new Uint32[SCREEN_WIDTH * SCREEN_HEIGHT];
    memset(buffer, 0x00, SCREEN_WIDTH*SCREEN_HEIGHT*sizeof(Uint32));
    for(int i=0; i < SCREEN_HEIGHT*SCREEN_WIDTH; i++) {
        buffer[i] = 0xABC123FF;
    }
    return buffer;
}

SDL_Surface* MakeSurface() {
    SDL_Surface* surface = SDL_LoadBMP("assets/charsheet_8x12.bmp");
    if(surface == nullptr) {
        throw std::runtime_error("Failed to load surface!");
    }
    return surface;
}

SDL_Rect* MakeTargetRect(int x, int y) {
    auto rect = new SDL_Rect();
    rect->w = CELL_PIXEL_WIDTH;
    rect->h = CELL_PIXEL_HEIGHT;
    rect->x = x * CELL_PIXEL_WIDTH;
    rect->y = y * CELL_PIXEL_HEIGHT;
    return rect;
}

SDL_Rect* MakeSrcRect(int index) {
    auto rect = new SDL_Rect();
    rect->w = CELL_PIXEL_WIDTH;
    rect->h = CELL_PIXEL_HEIGHT;
    rect->x = (index % CHARSHEET_WIDTH) * CELL_PIXEL_WIDTH;
    rect->y = (index / CHARSHEET_WIDTH) * CELL_PIXEL_HEIGHT;
    return rect;
}

Screen* Screen::MakeScreen() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cout << "Failed to init!" << std::endl;
    }

    SDL_Window* window = MakeWindow();
    Uint32* buffer = MakeBuffer();
    SDL_Renderer* renderer = MakeRenderer(window);

    SDL_Surface* surface = MakeSurface();
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_Rect* src_rect = MakeSrcRect(1);

    return new Screen(texture, buffer, window, renderer, src_rect);
}

/**
 * NDC = normalized device coordinates (parallel lines). NDC = [-1,1]
 * @param threeDCoords
 * @return
 */
glm::vec3 * Screen::makeProjectionMatrix(glm::vec3 threeDCoords) {
    // TODO: use my implementation
//    float aspectRatio = (float) SCREEN_WIDTH / (float) SCREEN_HEIGHT;
//
//    double focalLength = (float) 1 / std::tan(DEFAULT_FOV_RADIANS / 2);
//
//    glm::mat4 matProj = glm::mat4(
//            (aspectRatio * focalLength), 0, 0, 0,
//            0, focalLength, 0, 0,
//            0, 0, 1 , 1,
//            0, 0, 0, 0);

    glm::mat4 matProj = glm::infinitePerspectiveRH(
            DEFAULT_FOV_RADIANS,
            (float) SCREEN_WIDTH / (float) SCREEN_HEIGHT,
            0.1f);

    glm::vec4 outputVector = matProj * glm::vec4(threeDCoords, 1);
    return new glm::vec3(
            outputVector.x / outputVector.z,
            outputVector.y / outputVector.z,
            threeDCoords.z);
}

Screen::ScreenCoords * Screen::screenSpaceTransform(glm::vec3 * worldCameraTranslated){
    glm::vec2 pixelToCellDiscretizer = glm::vec2(
            SCREEN_CELL_WIDTH,
            SCREEN_CELL_HEIGHT);

    glm::vec2 finalOutput =
            glm::vec2(glm::vec2(*worldCameraTranslated) * pixelToCellDiscretizer);

    return new ScreenCoords((int) round(finalOutput.x), (int) round(finalOutput.y));
}
glm::vec3 Screen::cameraWorldTransform(glm::vec3 ndc) {
    return ndc + glm::vec3((float)0.5, 0.5, 0);
}

/**
 *
 * @param position position in world space of the camera
 * @param target is the position in world space (where origin is _not_ the camera)
 * @param up some vector pointing up
 * @return
 */
glm::mat4 makePointAtMatrix(glm::vec3 position, glm::vec3 target, glm::vec3 up) {
    // Calculate new forward direction (this is with origin at the position of the camera)
    glm::vec3 forwardWrtCamera = target - position;
    glm::vec3 normalizedForwardWrtCamera = glm::normalize(forwardWrtCamera);

    // Calculate the magnitude of the forward-ness of the up vector
    // (or the up-ness of the forward direction)
    float upForwardness = glm::dot(up, normalizedForwardWrtCamera);

    // scale back forwardness to how much up-ness it has
    glm::vec3 forwardnessScaled = normalizedForwardWrtCamera * upForwardness;
    // Calculate new up direction perpendicular to the forward direction
    glm::vec3 upWrtCamera = up - forwardnessScaled;

    glm::vec3 rightWrtCamera = glm::cross(upWrtCamera, normalizedForwardWrtCamera);

    return glm::mat4 {
            rightWrtCamera.x, rightWrtCamera.y, rightWrtCamera.z, 0,
            upWrtCamera.x, upWrtCamera.y, upWrtCamera.z, 0,
            normalizedForwardWrtCamera.x, normalizedForwardWrtCamera.y, normalizedForwardWrtCamera.z, 0,
            position.x, position.y, position.z, 1
    };
}

/**
 * NOTE: This only works for translation and rotation matrices
 *
 * Given some matrix:
 *  Ax, Ay, Az, 0,
 *  Bx, By, Bz, 0,
 *  Cx, Cy, Cz, 0,
 *  Tx, Ty, Tz, 1
 *
 *  Produce a matrix:
 *         Ax,        Bx,        Cx, 0,
 *         Ay,        By,        Cy, 0,
 *         Az,        Bz,        Cz, 0,
 *  -dot(T,A), -dot(T,B), -dot(T,C), 1
 *
 * @return
 */
glm::mat4 Screen::invert(glm::mat4 matrix) {
    float aX = matrix[0][0];
    float aY = matrix[0][1];
    float aZ = matrix[0][2];
    glm::vec3 A = glm::vec3(aX, aY, aZ);

    float bX = matrix[1][0];
    float bY = matrix[1][1];
    float bZ = matrix[1][2];
    glm::vec3 B = glm::vec3(bX, bY, bZ);

    float cX = matrix[2][0];
    float cY = matrix[2][1];
    float cZ = matrix[2][2];
    glm::vec3 C = glm::vec3(cX, cY, cZ);

    glm::vec3 T = glm::vec3(matrix[3][0], matrix[3][1], matrix[3][2]);

    return glm::mat4(
        aX, bX, cX, 0,
        aY, bY, cY, 0,
        aZ, bZ, cZ, 0,
        -(glm::dot(T, A)), -(glm::dot(T, B)), -(glm::dot(T, C)), 1
    );
}

glm::mat4 Screen::makeCameraViewMatrix(glm::vec3 cameraPosition) {
    glm::vec3 up = { 0.0, 1.0, 0.0 };
    glm::vec3 lookDirection = {0, 0.0, 1 };
    glm::vec3 target = cameraPosition + lookDirection;

    // TODO: use my implementation
    return glm::lookAt(cameraPosition, target, up);
//    glm::mat4 cameraMatrix = makePointAtMatrix(cameraPosition, target, up);
//    return glm::inverse(cameraMatrix);
};

/**
 *
 * @param x
 * @param y
 * @param z
 * @param counter
 */
void Screen::draw(glm::mat4 cameraViewMatrix, glm::vec3 point) {
    glm::vec3 viewedVertex = cameraViewMatrix * glm::vec4(point, 1);

    glm::vec3 * ndCoords = makeProjectionMatrix(viewedVertex);

    glm::vec3 worldCameraTranslated = cameraWorldTransform(*ndCoords);

    ScreenCoords * screenSpace = screenSpaceTransform(&worldCameraTranslated);

    SDL_Rect* rect = MakeTargetRect(screenSpace->x, screenSpace->y);
    SDL_RenderCopy(m_renderer, m_texture, m_src_rect, rect);
}

void
Screen::render(CameraModel * cameraPosition, int timer, std::vector<glm::vec3> points,
        RotationModel * rotationModel) {
    SDL_RenderClear(m_renderer);

    glm::mat4 cameraViewMatrix = makeCameraViewMatrix(cameraPosition->translation);

    for (glm::vec3 point : points) {
        draw(cameraViewMatrix, point);
    }

    // Render the changes above
    SDL_RenderPresent(m_renderer);
}

void Screen::initial_render() {
    //SDL_UpdateTexture(m_texture, nullptr, m_buffer, SCREEN_WIDTH*sizeof(Uint32));
    SDL_RenderClear(m_renderer);
    SDL_RenderPresent(m_renderer);
}
